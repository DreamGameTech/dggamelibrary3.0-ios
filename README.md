简体中文 |  [English](#EN) 

--
<a id="CN"> </a>


## DGGameLibray-iOS 是DreamGame Technology 公司游戏产品的SDK，供平台商对接我司游戏使用

## 该SDK包含DG的游戏为： *百家乐、龙虎、牛牛、轮盘、骰宝*

## 支持iOS 9以上，iPhone、iPad的 ~~i386 x86_64 armv7~~ arm64架构, 支持TestFilght包

### 文档修改日志

版本号 | 修改内容 | 修改日期
----|------|----
3.3.1 | iOS17兼容| 2023-09-20
3.3.0 | 安全性更新| 2023-01-11
3.2.1 | iOS16 兼容| 2022-09-16
3.2.0 | 安全性更新| 2022-07-09
3.1.3 | 版本优化| 2022-04-12
3.1.2 | 新增虚拟机架构| 2021-12-10
3.1.1 | 版本优化| 2021-12-02
3.1.0 | 1.SDK视频库分离，需加入工程打包<br/><b>NodeMediaClient.framework</b> <br/>2. 修复 SDK可上架App Store| 2021-06-18
3.0.6 | 优化聊天表情| 2021-01-07
3.0.5 | iPhone 12系列 兼容| 2020-11-26
3.0.4 | 添加国际化| 2020-09-26
3.0.3 | 视频加密、请所有用户强制更新| 2020-08-29
3.0.2 | 修复骰宝结果bug| 2020-02-21
3.0.1 | 修复背景声音| 2020-01-16
3.0.0 | 新版DG UI| 2020-01-15


###  常见错误

#### 1.假设cocos项目 Other Linker Flags  不能使用-ObjC库的话,  使用-force_load
```
bug show：
+[DGLoginBean DGmj_objectWithKeyValues:]: unrecognized selector sent to class 0x106a0c358
```


  
### 对接方式
    
#### 1. 添加配置
(1) TARGETS：
             framework使用视频框架，需引入系统框架<b>VideoToolbox.framework</b>  


(2) info.plist ：
             支持HTTP的请求
             
```
App Transport Security Settings     (Type:Dictionary)   
-> Allow Arbitrary Loads (Type:Boolean Value:YES)
```

(3) Build Settings ：
             由于framework的配置， 需要添加配置
             
```
Linking 
    >Other Linker Flags    添加： -ObjC
    
添加库
TARGETS->build Phases->Link Binary With Libraries  添加库Libresolv    
```      

### 2.引入SDK
相关文件在DGSDK 目录内，请前往查看。    
需要把DGSDK.bundle、DGSDKResources.bundle、DGSDK.framework 拖入工程，Add to targets ☑️(打勾)  
<b>备注DGSDK.bundle 在DGSDK.framework内</b>

```
DGSDK.bundle
DGSDKResources.bundle
DGSDK.framework 
NodeMediaClient.framework <3.1.0 分离视频库>
```     
#### 3. 代码接入SDK

(1) 引入SDK文件：

```
#import <DGSDK/DGSDKHandle.h>  
#import <DGSDK/DGGameId.h>  
```

(2) 监听SDK消息通知    <b>---可选</b>
> DGSDKWILLENTERGAME：即将进入SDK   
 DGSDKENTERGAME：已经进入SDK    
 DGSDKEXITEDGAME  ：已经退出SDK    
 DGSDKENTEREDGAMEFAIL   ：进入SDK失败 -- 有值回调
 
> 数据加载失败的回调，i 有三个类型   
> key:DGError  
> 0 --- socket链接失败   
> 1 --- 会员初始化失败   
> 2 --- 会员账号暂停   
> 3 --- 资源加载失败	   
	



(3) 调用SDK
	
> token:(String)                  需要登录用户token  
VC:(UIViewControler)     当前的ViewController  
gameID:(Int)                    传游戏ID    ID表详见DGGameId.h文案  
isLoadDGGame:(Bool)    显示SDK加载Toast    YES显示， False不显示  
 minBet:(Int)                      余额最小下注额度，小于设定额度提示充值，0不限制  
domains:(String)          获取配置文件的字符串， 对接登录api会提供





> gameID注释   
DGBACCARAT： 百家乐    
DGDRAGON：  龙虎   
DGBULL：    牛牛   
DGROULETTE：    轮盘  
DGSICBO：    骰宝   
 
```
- (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
                 withGameID:(int)gameID
      withLoadDGGameLoading:(BOOL)isShowHUD
                 withMinBet:(int)minBet
                   withData:(NSString *)domains;
```

代码演示

```
  [DGSDKHANDLE DGSDKLoginWithToken:token
                withViewController:self
                        withGameID:DGBACCARAT
             withLoadDGGameLoading:YES 
                        withMinBet:50
                          withData:dataConfig];

```

(4) 国际化 <b>--可选</b>
>（默认）简体中文  DGLangSetCN <br> 繁体中文  DGLangSetHK <br>英文   DGLangSetEN   

代码演示

```
    [DGSDKHANDLE DGSDKLangSet:DGLangSetHK];
```   
---
---
---
[简体中文](#CN) | English

--
<a id="EN"></a>


## DGGameLibray-iOS is the SDK product of DreamGame Technology, allowing merchant or operator to integrate with our product for usage. 


## The SDK product includes DreamGaming<br> *Baccarat, DragonTiger, Bull Bull, Roulette and SicBo*



## Can be support by IOS9 and above, iPhone, iPad and  ~~i386 x86_64 armv7~~ arm64 structure, support TestFilght.



### Change Log

VersionNumber | Updated Content | Updated DateTime
----|------|----
3.3.1 | iOS17 compatible| 2023-09-20
3.3.0 | Version optimization| 2023-01-11
3.2.1 | iOS16 compatible| 2022-09-16
3.2.0 | security update| 2022-07-09
3.1.3 | Version optimization| 2022-04-12
3.1.2 | New add virtual machine architecture| 2021-12-10
3.1.1 | Version optimization| 2021-12-02
3.1.0 | 1.The SDK video library is separated and needs to be added to the project package<br/> <b>NodeMediaClient.framework</b> <br/>2. Fix the SDK can be put on the App Store| 2021-06-18
3.0.6 | Optimize chat emoticons| 2021-01-07
3.0.5 | iPhone 12 Compatible| 2020-11-26
3.0.4 | Globalization| 2020-09-26
3.0.3 | Video encryption| 2020-02-21
3.0.2 | Sicbo Result Bug| 2020-02-21
3.0.1 | Background music| 2020-01-16
3.0.0 | New DG UI| 2020-01-15

###  Common mistakes

#### 1.If the cocos project Other Linker Flags cannot use the -ObjC library, use -force_load
```
bug show：
+[DGLoginBean DGmj_objectWithKeyValues:]: unrecognized selector sent to class 0x106a0c358
```


  
  
### Integration Method
    
#### 1. Add configuration
(1) TARGETS：
             Due to the configuration of the framework, you need to add configuration<br><b>VideoToolbox.framework</b>  


(2) info.plist ：
             Support HTTP request
             
```
App Transport Security Settings     (Type:Dictionary)   
-> Allow Arbitrary Loads (Type:Boolean Value:YES)
```

(3) Build Settings ：
             Due to the configuration of the framework, you need to add configuration
             
```
Linking 
    >Other Linker Flags    Add： -ObjC
    
Add framework
TARGETS->build Phases->Link Binary With Libraries   Add framework "Libresolv"
```      

### 2.Import SDK files
The relevant files are in the DGSDK directory, kindly check.    
Need to import DGSDK.bundle、DGSDKResources.bundle、DGSDK.framework to the project，Add to targets ☑️(Tick).  <br/>
<b>Notice: bundle included into framework</b>

```
DGSDK.bundle
DGSDKResources.bundle
DGSDK.framework 
NodeMediaClient.framework <3.1.0 Separate video library>
```     
#### 3. Code access SDK

(1) Import SDK files：

```
#import <DGSDK/DGSDKHandle.h>  
#import <DGSDK/DGGameId.h>  
```

(2) Monitor SDK notification  ---Optional
> DGSDKWILLENTERGAME：Going to enter SDK   
 DGSDKENTERGAME：Entered SDK    
 DGSDKEXITEDGAME  ：Exited SDK    
 DGSDKENTEREDGAMEFAIL   ：Enter SDK Failure --Callback value
 
> Callback of data downloading failure, "I" have 3 types   
> key:DGError  
> 0 --- socket connection failure  
> 1 --- Member initialization failure  
> 2 --- Member account suspended  
> 3 --- Sources uploading failure	   
	



(3) Transfer SDK
	
> token:(String)                  Member login needed Token  
VC:(UIViewControler)     Current ViewController  
gameID:(Int)                    Game ID transfer, ID table looking into DGGameid.h detail  
isLoadDGGame:(Bool)    Show SDK uploaded Toast, YES for Display, Failed for Not Display  
 minBet:(Int)                      Leftover minimum bet, less than minimum bet setting show Reload, 0 not restricted 
domains:(String)          Obtain String of configuration files, integrate Login API will be provided





> gameID Detail   
DGBACCARAT： Baccarat    
DGDRAGON：  Dragon   
DGBULL：    Bull   
DGROULETTE：    Roulette  
DGSICBO：    Sicbo   
 
```
- (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
                 withGameID:(int)gameID
      withLoadDGGameLoading:(BOOL)isShowHUD
                 withMinBet:(int)minBet
                   withData:(NSString *)domains;
```

Code Sample

```
  [DGSDKHANDLE DGSDKLoginWithToken:token
                withViewController:self
                        withGameID:DGBACCARAT
             withLoadDGGameLoading:YES 
                        withMinBet:50
                          withData:dataConfig];

```

(4)Globalization <b>---Optional</b>
>（默认）简体中文  DGLangSetCN <br> 繁体中文  DGLangSetHK <br>英文   DGLangSetEN   

Code Sample

```
    [DGSDKHANDLE DGSDKLangSet:DGLangSetHK];
```  
