//
//  ViewController.m
//  DGSDKDemo
//
//  Created by DG on 12/11/18.
//  Copyright © 2018 DG. All rights reserved.
//

#import "DFViewController.h"
#import "AFNetworking.h"


#import <DGSDK/DGSDKHandle.h>
#import <DGSDK/DGGameId.h>

@interface DFViewController ()

@end

@implementation DFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //监听SDK推送=----- 可选择
    //准备进入SDK
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKWillEnterGame) name:@"DGSDKWILLENTERGAME" object:nil];
    //进入SDK -- token登录成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKEnteredGame) name:@"DGSDKENTEREDGAME" object:nil];
     //已经退出SDK
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKExitedGame) name:@"DGSDKEXITEDGAME" object:nil];
    
    //进入SDK失败
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKEnterGameFail:) name:@"DGSDKENTEREDGAMEFAIL" object:nil];
}

- (void)logoutSDK{
     [DGSDKHANDLE DGSDKAccountLogout];
}
#pragma mark - 监听SDK推送
- (void)DGSDKWillEnterGame{
    NSLog(@"即将进入SDK");
}
- (void)DGSDKEnteredGame{
    NSLog(@"已经进入SDK ");
}
//失败回调

/*
 
 key:DGError
 
 数据加载失败的回调，i 有三个类型
 SocketNetError = 0;    socket链接失败
 MemberInitError = 1;   会员初始化失败
 MemberStop = 2;        会员账号暂停
 FileLoadError = 3;     资源加载失败
 SoundLoadError = 4;     背景声音加载失败
 */
- (void)DGSDKEnterGameFail:(NSNotification *)notifier{
    NSDictionary *dicError = notifier.object;
    int code = [dicError[@"DGError"] intValue];
    NSLog(@"SDK失败状态码。。。。 %d",code);
}

- (void)DGSDKExitedGame{
    NSLog(@"已经退出SDK");
}

#pragma mark - Action
- (IBAction)start_action:(id)sender {
    
    int lobbyType = DGBACCARAT;
    switch ([sender tag]) {
        case 0:
            lobbyType = DGBACCARAT;
            break;
        case 1:
            lobbyType = DGDRAGON;
            break;
        case 2:
            lobbyType = DGBULL;
            break;
        case 3:
           lobbyType = DGROULETTE;
           break;
        case 4:
           lobbyType = DGSICBO;
           break;
        
    }
    //请求获取Token， 请求获取配置文件game_info.json
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //1. 请求获取配置文件game_info.json， 对接登录api会提供
    [manager GET:@"https://static.dg012.com/game_info.json" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"获取到配置文件:%@",dic);
        if (responseObject) {
            //2. 获取token
            [manager POST:@"https://www.20266666.com/game/h5/" parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject1) {
                NSDictionary *dic = responseObject1;
                if (dic) {
                    NSString *token = dic[@"token"];
//                    token = @"37ee81945d164f519004b62ae493022e";
                    if (token!=nil) {
                        //3.接入SDK
                        
                        /*
                         token:(String)       需要登录用户token
                         VC:(UIViewControler) 当前的ViewController
                         gameID:(Int)         传游戏ID    ID表详见DGGameId.h文案
                         isLoadDGGame:(Bool)  显示SDK加载Toast    YES显示， False不显示
                         minBet:(Int)         最小下注额度，0不限制
                         dataConfig:(String)  获取配置文件的字符串， 对接登录api会提供
                         */
                        [DGSDKHANDLE DGSDKLoginWithToken:token withViewController:self withGameID:lobbyType withLoadDGGameLoading:YES withMinBet:50 withData:responseObject];
                        //繁体
                        //[DGSDKHANDLE                              DGSDKLangSet:DGLangSetHK];

                        //英文
                        //       [DGSDKHANDLE                              DGSDKLangSet:DGLangSetEN];
                        //简体中文
                        [DGSDKHANDLE                              DGSDKLangSet:DGLangSetCN];
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {}];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {}];
}


@end
