//
//  main.m
//  DGSDKDemo
//
//  Created by DG on 12/11/18.
//  Copyright © 2018 DG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
