//
//  AppDelegate.h
//  DGSDKDemo
//
//  Created by DG on 12/11/18.
//  Copyright © 2018 DG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

